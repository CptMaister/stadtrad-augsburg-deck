# Stadtrad-Augsburg-Deck

Aktuelle Version: 0.4.4

Nicht viel zu speichern hier, dient nur als Ablage der neusten Version des Decks, später vielleicht mehr.

Die offizielle Auswertung der Stadtratswahl vom 15.03.2020 gibt es hier: [augsburg.de](https://www.augsburg.de/fileadmin/user_upload/verwaltungswegweiser/buergeramt/wahlen/kommunalwahlen/2020/sr/index.html#w_8013_9006)